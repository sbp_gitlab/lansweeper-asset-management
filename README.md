# Lansweeper Asset Management

PRTG sensors to retrieve asset information from Lansweeper

## Introduction

This project provides a couple of PRTG sensors for collecting and displaying data from a cloud hosted Lansweeper instance.

•	A REST Custom Sensor template that shows the number of assets being tracked by the system. This is a simple example that can be changed to pull data from any available endpoint from the Lansweeper API

•	An EXE/Script sensor that includes a parameterised PowerShell script that retrieves and parses more complex data from the Lansweeper API. This can be used to show the number of a specific asset type (e.g., “printers”, “servers” CCTV cameras”, etc.) being managed. Again, this script can be edited to retrieve different data, as needed.


## Installation & Usage

For the REST sensor, copy the included Lansweeper_AssetCount.template to the rest custom sensor folder on your PRTG server (default - C:\Program Files (x86)\PRTG Network Monitor\Custom Sensors\rest).

Add the sensor to the Lansweeper device and add the POST data and the authentication header information. You will need the “SiteID” and “authentication token” from the Lansweeper Config page (more details can be found in the Lansweeper API documentation - https://docs.lansweeper.com/docs/api/intro).

For the EXE/Script sensor, copy the included Lansweeper_DeviceCount.ps1 file to the EXEXML custom sensor folder on your PRTG server (default - C:\Program Files (x86)\PRTG Network Monitor\Custom Sensors\EXEXML).

Add the sensor to the Lansweeper device and provide values for the three parameters (-siteID, -token,  -assetType). SiteID and token can be found in the Lansweeper Config page (more details can be found in the Lansweeper API documentation - https://docs.lansweeper.com/docs/api/intro). The assetType value is the type string shown on the asset details page in Lansweeper, for example, “camera”, “printer”, etc. Enclose the search word(s) in quotes.

## More Information

More information about integrating PRTG with Lansweeper, and how to configure the sensors, can be found in this blog article: https://blog.paessler.com/prtg-lansweeper-taking-the-un-out-of-unknown 
